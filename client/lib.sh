. "$DIR/lib/logging.sh" || exit 230;
. "$DIR/lib/fingerprint.sh" || exit 231;

decide_domain() {
	domain="localhost:3000";
	[ -f "$DIR/data/domain" ] && domain="$(cat "$DIR/data/domain")";
}

# We usually want http on direct localhost connections.
decide_protocol() {
	protocol="https";
	case $domain in
		localhost:*)
			protocol="http";
			;;
	esac
}

send_remote() {
	args="$(echo $@ | tr ' ' '_')";
	curl --header "fingerprint: $(generate_fingerprint)" --silent --show-error --fail "$protocol://$domain/$args" | tr -d '\r';
}

main() {
	decide_domain;

	while true; do
		case $1 in
			--domain)
				shift;
				domain="$1";
				;;
			--*)
				fail "Unknown command: $1";
				;;
			*)
				break;
				;;
		esac
		shift;
	done

	decide_protocol;

	key_exchange;

	send_remote "$@";
}
