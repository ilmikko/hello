mkdir -p "$DIR/data";

keyfile="$DIR/data/key";
countfile="$DIR/data/count";

save_count() {
	echo "$count" > "$countfile";
}

shacmd="";
command -v sha512 2>/dev/null 1>&2 && shacmd=sha512;
command -v sha512sum 2>/dev/null 1>&2 && shacmd=sha512sum;

[ -n "$shacmd" ] || fail "Cannot determine SHA512 sum command!";

SHA512() {
	printf "$@" | $shacmd | awk '{ print $1 }';
}

generate_fingerprint() {
	count=$((count + 1));
	save_count;

	echo "$count/$(SHA512 "$key/$(SHA512 "$count")")";
}

key_exchange() {
	if [ -f "$keyfile" -a -f "$countfile" ]; then
		key="$(cat "$keyfile")";
		count="$(cat "$countfile")";
	else
		log "Fetching new key...";
		key="$(send_remote key)" || fail "Key exchange failed.";
		key="$(echo "$key" | tr -d "\r\n")";
		count=0;

		log "Key: $key";
		echo "$key" | tr -d "\r\n" > "$keyfile";
		echo "$count" > "$countfile";
	fi
}
