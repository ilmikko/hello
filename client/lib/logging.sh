fail() {
	echo "$@" 1>&2;
	exit 1;
}

log() {
	echo "$@" 1>&2;
}
