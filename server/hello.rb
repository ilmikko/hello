#!/usr/bin/env ruby

$DIR = File.dirname(__FILE__);
$LOAD_PATH << "#{$DIR}/lib";

require 'context';
require 'hosts';
require 'keys';
require 'server';

require 'datahandler';

$host = 'localhost';
$port = 3000;

names = {};

def determine_trust(context)
  # Determine the amount of trust for this IP.
  # There are four cases that can happen.
  # 1. Fingerprint is anonymous. In this case, there is zero trust.
  # 2. Fingerprint is set, but invalid. In this case, there is zero trust.
  # 3. Fingerprint is set and matches. In this case, there is known trust.
  # 4. Fingerprint is set and matches, and the IP is whitelisted (local). There is full trust.
  trust = :none;
  fingerprint = context.headers['fingerprint'];
  if !fingerprint.nil?
    if Key.valid?(fingerprint, context);
      trust = :known;

      if context.ip == '::1'
        trust = :full;
      end
    end
  end

  return trust;
end

def log_request(context, trust)
  trusts = {
    none:  "[1;33mUNKNOWN[0;33m",
    known: "[1;32mKNOWN  [m",
    full:  "[1;31mFULL   [1;31m",
  };
  puts("#{trusts[trust]} [#{context.ip}] #{context.request}[m");
end

def run_command(command, context)
  trust = determine_trust(context);

  log_request(context, trust);

  if trust == :full;
    # Fully trusted commands.
    case command.first
    when "help" # Show this help message.
      help = File.read(__FILE__).split(/\r?\n/).delete_if{ |line|
        line[/^\s*when\s.*#.*$/].nil?;
      };
        help.map!{ |line|
          command = "#{line[/["']\w*["']/]}".gsub(/^["']|["']$/, '');
          comment = "#{line[/#[^#]*$/]}".sub(/^\s*#\s*/, '');
            "#{command} - #{comment}";
        };
        context.send(help.join("\r\n"));
        return;
    when "load" # Load data handlers from files.
      DataHandler.load_all_handlers;
      context.send("Loaded!");
      return;
    when "save" # Save data handlers to files.
      DataHandler.save_all_handlers;
      context.send("Saved!");
      return;
    when "hosts" # List all host mappings.
      context.send(Hosts.all.map{|list| list.join(' - ')}.join("\r\n"));
      return;
    when "shutdown" # Shut down the server.
      context.send("Shutting down.");
      exit;
    end
  end

  if trust == :full or trust == :known;
    # Known commands.
    case command.first
    when "countries" # Show countries this fingerprint has been in.
      context.send("TODO", status="500 TODO");
      return;
    when "iam" # IP creating a named record for them.
      name = command[1];
      if Hosts.set(name, context.ip, context.headers['fingerprint']);
        context.send("Okay #{name}");
      else
        context.send("Sorry #{context.ip}");
      end
      return;
    when "who" # IP asking for a named record.
      context.send("#{Hosts.get(command[1])}");
      return;
    end
  end

  case command.first
  when nil, "hello" # Simple heartbeat from an IP. Default.
    command[0] = nil;
    context.send("Hello #{context.ip}#{command.join(' ')}");
    return;
  when "key" # IP requesting a new key.
    context.send("#{Key.generate}");
    return;
  when "trust" # IP requesting whether we trust them or not.
    context.send("#{trust.to_s} #{context.ip}");
    return;
  else
    context.send("Bad request", status="400 BAD REQUEST");
    return;
  end
end

Server.new($host, $port){ |context|
  run_command(context.path[1..-1].split('_'), context);

  context.close;
};
