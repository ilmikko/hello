require 'datahandler';

# Keeps a track of which ips have been which hosts.

class Hosts
  @@hosts = DataHandler.new("hosts");
  @@owners = DataHandler.new("hosts/owners");

  def self.all()
    @@hosts.map{ |name, ips|
      [name] + ips;
    };
  end

  def self.get(name)
    return nil if !@@hosts.key? name;
    return @@hosts[name].last;
  end

  def self.set(name, ip, fingerprint)
    secret = Key.fingerprint_to_secret(fingerprint);
    # Check the owner of this name.
    if !@@owners.key? name or @@owners[name] == secret;
      @@owners[name] = secret;
      @@hosts[name] = [] if !@@hosts.key? name;
      # Only push if the ip changes - otherwise it's already there.
      @@hosts[name] << ip if @@hosts[name].last != ip;
      return true;
    else
      return false;
    end
  end
end
