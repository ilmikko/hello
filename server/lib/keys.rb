require 'digest';
require 'datahandler';

class Key
  @@secrets = DataHandler.new("secrets");
  @@known_count = DataHandler.new("known_count");
  @@known_ips = DataHandler.new("known_ips");

  def self.generate
    salt = "This is the salt for hello, running on #{$host}:#{$port}.";
    secret = Digest::SHA512.hexdigest "#{salt}#{rand}";

    @@secrets[secret] = 0;
    @@known_count[secret] = 0;
    @@known_ips[secret] = {};

    return secret;
  end

  def self.generate_fingerprint(count, secret)
    return Digest::SHA512.hexdigest("#{secret}/#{Digest::SHA512.hexdigest("#{count}")}");
  end

  def self.hash_to_secret(hash, count)
    @@secrets.each{ |secret, _|
      if generate_fingerprint(count, secret) == hash;
        return secret;
      end
    };
    return nil;
  end

  def self.fingerprint_to_secret(fingerprint)
    return hash_to_secret(fingerprint.sub(/^\d+\//, ''), count_from_fingerprint(fingerprint));
  end

  def self.count_from_fingerprint(fingerprint)
    return fingerprint[/^\d+/].to_i;
  end

  def self.valid?(fingerprint, context=nil)
    secret = fingerprint_to_secret(fingerprint);
    return false if secret.nil?;

    # TODO: This does not belong here
    # This increases the logins for this secret.
    if !context.nil?
      @@known_ips[secret] = {} if !@@known_ips.key? secret;
      @@known_ips[secret][context.ip] = 0 if !@@known_ips[secret].key? context.ip;
      @@known_ips[secret][context.ip] += 1;
    end

    # Reject unknown fingerprints.
    return false if !@@known_count.key? secret;

    # Reject old fingerprints.
    count = count_from_fingerprint(fingerprint);
    return false if not count > @@known_count[secret];

    @@known_count[secret] = count;
    @@secrets[secret] += 1;
    # TODO: We should cache the next couple (10) of fingerprints
    # so that we don't have to loop through ALL the keys to find which key they belong to
    return true;
  end

  public

  attr_reader :secret;

  def initialize(secret)
    @secret = secret;
  end
end
