require 'socket';

class Server
  def initialize(host, port)
    @server=TCPServer.new(host, port);

    loop{
      context = Context.new(@server.accept);

        Thread.new{
          begin
          proc.(context);
          rescue Exception => e
            puts "Exception: #{e}";
            begin
            context.send("Bad request", status="400 BAD REQUEST");
            context.close;
            rescue Exception => e
              puts "Could not close context: #{e}";
            end
          end
        }
    }
  end
end
